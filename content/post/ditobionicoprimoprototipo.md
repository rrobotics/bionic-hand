---
title: Dito bionico 
subtitle: primo prototipo 
---

Il *primo prototipo* del dito bionico presenta **due gradi di libertà**, ovvero due movimenti indipendenti, che simulano le falangi delle dita. Per ciascun grado di libertà vi è un **singolo motore**. Nel corso del progetto, per effettuare i fori che permettessero il passaggio dei fili, si è considerata *un'angolazione* pari a **90°**. 

Sono presenti poi quattro cavi: due cavi fanno ruotare la **ruota della prima forcella**, ovvero il collegamento tra la falange prossimale e il palmo, mentre gli altri due permettono **l'apertura e chiusura del dito** scorrendo nelle ruote delle falangi media e ditale. 

Dal moemnto che l'utilizzo di due gradi di libertà sarebbe risultato superfluo per lo scopo principale, si è deciso di **ridurre** i gradi di libertà. In questo modo abbiamo potuto creare un secondo prototipo

