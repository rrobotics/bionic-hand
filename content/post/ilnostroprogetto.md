---
title: IL NOSTRO PROGETTO
---

La mano robotica è ben lontana dall'essere definita una *"copia omologica"*. Abbiamo infatti progettato un modello simulato, il quale presenta tre dita ed un palmo diverso da quello umano. Tuttavia, la funzione che si è voluta riprodurre è quella della apertura e chiusura della mano, la quale avviene in modo controllato, grazie a dei motori e un sistema di ritorno di forza. 

Inanzitutto, abbiamo preso come punto focale lo studio dei **gradi di libertà di un dito**, ovvero i movimenti indipendenti che ciascuna falange può eseguire. Abbiamo realizzato digitalmente il prototipo della mano attraverso il programma *"Blender"*, successivamente grazie anche all'utilizzo della **stampante 3D** della scuola, abbiamo stampato il progetto. In questo modo abbiamo potuto vedere in maniera realistica la mano bionica e quindi valutare **concretamente** il funzionamento e i cambiamenti da eseguire. 
Tutto ciò ci ha permesso di realizzare un secondo prototipo migliorato. 

Di seguito, abbiamo realizzato che prima di creare interamente la mano, dobbiamo concentrarci sulla ideazione di ciascun dito. Su questo pensiero abbiamo realizzato due prototipi di **dito bionico**, il quale sarebbe andato a costituire la mano bionica finale.
