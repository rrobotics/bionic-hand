---
title: Dito bionico
subtitle: secondo prototipo
---

Con il *secondo prototipo* abbiamo eseguito numerosi cambiamenti. Infatti il secondo dito presenta un **solo grado di libertà** e il movimento del dito è ora gestito unicamente da un **unico motore**. Con l'utilizzo di un grado di libertà, si è utilizzato un singolo cavo, fisso sulla ruota della falange ditale e che scorre lungo le ruote delle **falangi medie e prossimali**. Abbiamo poi notato che l'angolazione precendete, ovvero quella di 90° non fosse del tutto *ottimale*. Infatti abbiamo studiato diverse angolazioni per poter aumentare l'angolazione dei fori precedente, ovvero: *7,5°, 15° e 30°*. Dopo diverse prove si è deciso che la scelta migliore fosse quella di aumentare di un'angolazione pari a **7,5°**. 
