---
title: La categoria scelta
---

Abbiamo scelto la *categoria A*, che vede come tema la **robotica protesica**. Abbiamo preso spunto dall'ideea di un *ex studente* della scuola, il quale ha basato la propria tesina del quinto anno sl medesimo tema, abbiamo approfondito l'argomento e realizzato un modello *quasi* funzionante. L'opinione condivisa dal gruppo è come il corpo umano sia la macchina **più complessa**,ma allo steso tempo perfetta sulla faccia della terra. Gli **infinti meccanismi** e processibiologici che in tempi rapidissimi si sviluppano in essa appaiono incredibili. 
