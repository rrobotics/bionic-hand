## Il gruppo RainerumRobotics 

Ciao a tutti, siamo i ragazzi del [RainerumRobotics](https://www.rainerum.it/index.php/scuole/robotica) della scuola Rainerum Bolzano. Quest'anno abbiamo deciso di partecipare alle *Olimpiadi della Robotica* scegliendo la categoria A: protesi robotiche e il nostro progetto si chiama:**"Bionic Hand**. 
Per leggere di più sul nostro team, visita la sezione [La nostra squadra](https://rrobotics.gitlab.io/bionic-hand/page/about/)

<!--![Foto a CASO](/static/static/protesi-removebg-preview.png)-->
