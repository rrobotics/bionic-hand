---
title: La nostra squadra
comments: false
---

Ciao a tutti! Siamo i ragazzi del gruppo RainerumRobotics di Bolzano. Quest'anno rappresenteremo la nostra scuola il Rainerum, alle Olimpiadi della Robotica. 

### Federica
Per Federica, "Bionic hand" è il primo progetto proposto alle Olimpiadi della Robotica di cui si occupa. Negli scorsi anni, ha contribuito ad altri lavori dei robotici. Il suo ruolo nella squadra è stato quello di registare i vari filmati e di scrivere la documentazione del lavoro svolto. 

### Alessia
Alessia è componente del gruppo Rainerum Robotics dall'ultimo anno del biennio e ha già partecipato ad alcune competizioni. In questo lavoro, ha progettato il design in 3D della mano tramite l'utilizzo del programma "Blender".

### Patrick
Patrick fa parte della squadra di robotica da tre anni e ha preso parte a diversi progetti. Nell'ideazione della mano robotica, Patrick ha dato il suo contributo...
